export function validateSsn(value) {

    const regExp = /^[0-9]\d{2}-\d{2}-\d{4}$/;
    const res = regExp.test(value);
    const result = (res === true && value.length <= 11);
    return result;

}

function validString(item) {
    return typeof item === 'string' && item.trim().length > 1
}

export function validateForm(value, firstName, lastName, address) {

    return (!validString(firstName) || !validString(lastName) || !validString(address) || !validString(value)) ? true : false;

}